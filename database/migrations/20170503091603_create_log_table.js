'use strict';

exports.up = function up(knex, Promise) {
  return knex.schema.createTableIfNotExists('log', table => {
    table.increments('id');

    table.integer('user_id').unsigned();
    table.integer('mosque_id').unsigned();

    table.timestamp('created_at', 'UTC').defaultTo(knex.fn.now());

    table.string('action', 255);
    table.json('data');
  });
};

exports.down = function down(knex, Promise) {
  return knex.schema.dropTableIfExists('log');
};
