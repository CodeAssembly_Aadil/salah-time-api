'use strict';

exports.up = function up(knex, Promise) {
  return knex.schema
    .raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
    .then(() => {
      return knex.schema.createTableIfNotExists('users', table => {
        table.uuid('uuid').defaultTo(knex.raw('uuid_generate_v4()'));

        table.string('firstname', 255);
        table.string('lastname', 255);

        table.string('organization', 255);

        table.string('phone_number', 64).unique();

        table.boolean('accepted_terms').defaultTo(false);

        table.string('otp', 128).nullable();
        table.timestamp('otp_created_at');

        table.string('token', 128).nullable();
        table.timestamp('token_created_at', 'UTC');

        table.boolean('locked').defaultTo(false);
        table.boolean('activated').defaultTo(false);

        table.timestamp('created_at', 'UTC').defaultTo(knex.fn.now());
        table.timestamp('updated_at', 'UTC').defaultTo(knex.fn.now());
      });
    });
};
// };
// exports.up = function up(knex, Promise) {
//   return knex.schema.createTableIfNotExists('users', table => {
//     // table.increments('id');
//     return knex
//       .raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
//       .then(() => {
//         // set up tables
//         table.string('firstname', 255);
//         table.string('lastname', 255);

//         table.string('organization', 255);

//         table.string('phone_number', 64).unique();

//         table.boolean('accepted_terms').defaultTo(false);

//         table.string('otp', 128).nullable();
//         table.timestamp('otp_created_at');

//         table.string('token', 128).nullable();
//         table.timestamp('token_created_at', 'UTC');

//         table.boolean('locked').defaultTo(false);
//         table.boolean('activated').defaultTo(false);

//         table.timestamp('created_at', 'UTC');
//         table.timestamp('updated_at', 'UTC');
//       });
//   });
// };

exports.down = function down(knex, Promise) {
  return knex.schema.dropTableIfExists('users');
};
