'use strict';

exports.up = function up(knex, Promise) {
  return knex.schema.createTableIfNotExists('mosques', table => {

    table.uuid('uuid').defaultTo(knex.raw('uuid_generate_v4()'));
    
    table.string('slug', 255).unique();

    table.string('name', 255);

    table.string('building', 255);
    table.string('street_number', 32);
    table.string('route', 255);
    table.string('sublocality', 255);
    table.string('locality', 255);
    table.string('postal_code', 32);
    table.string('province', 128);
    table.string('country', 128);

    // Google maps PlaceID
    table.string('place_id', 255).unique();

    // PostGis longitude, latitude
    table.specificType('location', 'GEOGRAPHY(POINT,4326)');

    table.json('times');
    table.timestamp('times_updated_at', 'UTC').defaultTo(knex.fn.now());

    table.timestamp('deleted_at', 'UTC').nullable();
    table.timestamp('created_at', 'UTC').defaultTo(knex.fn.now());
    table.timestamp('updated_at', 'UTC').defaultTo(knex.fn.now());
  });
};

exports.down = function down(knex, Promise) {
  return knex.schema.dropTableIfExists('mosques');
};
