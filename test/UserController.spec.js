var Chai = require( 'chai' );
var Axios = require( 'axios' );

var expect = Chai.expect;

const USER_DATA = {
	firstname: 'Test',
	lastname: 'User',
	organization: 'Organization',
	phone_number: '0720000001'
};

const USER_DATA_UPDATE = {
	firstname: 'TestUpdate',
	lastname: 'UsertUpdate',
	organization: 'OrganizationtUpdate',
	phone_number: '0720000001'
};

const USER_DATA_MALFORMED = {
	firstname: '', // empty field
	// missing field
	organization: 'Or', // missing field
	phone_number: '0720000001' // duplicate
};

let userID = null;
let token = null;
let fakeUserID = 'xyz';

const RESOURCE_URL = 'http://salahtime.develop/users';

describe( 'UserController', function () {

	describe( 'store: Create User', function () {

		it( 'Returns status 201 for good data', function ( done ) {

			Axios.post( RESOURCE_URL, USER_DATA )
				.then( function ( response ) {

					expect( response.status ).to.equal( 201 );

					expect( response.data.firstname ).to.equal( USER_DATA.firstname );
					expect( response.data.lastname ).to.equal( USER_DATA.lastname );
					expect( response.data.organization ).to.equal( USER_DATA.organization );
					expect( response.data.phone_number ).to.equal( USER_DATA.phone_number );

					userID = response.data.id;

					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 422 for malformed data', function ( done ) {

			Axios.post( RESOURCE_URL, USER_DATA_MALFORMED )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 422 );
					done();
				} );
		} );
	} );

	describe( 'AuthController', function () {

		describe( 'requestOTP: Authorize User', function () {

			it( 'Returns status 401 for invalid phone_number', function ( done ) {

				Axios.post( `${RESOURCE_URL}/authorize`, { phone_number: 'fake_phone_number' } )
					.then( function ( response ) {
						done(response);
					} )
					.catch( function ( error ) {
						expect( error .response.status ).to.equal( 401 );
						done( );
					} );
			} );

			it( 'Returns status 202 for existing User with valid phone_number', function ( done ) {

				Axios.post( `${RESOURCE_URL}/authorize`, { phone_number: USER_DATA.phone_number } )
					.then( function ( response ) {
						expect( response.status ).to.equal( 202 );
						done();
					} )
					.catch( function ( error ) {
						done( error );
					} );
			} );
		} );

		describe( 'activate: Activate User', function () {

			it( 'Returns status 401 for invalid otp', function ( done ) {
				Axios.post( `${RESOURCE_URL}/activate`, { otp: '5678', phone_number: USER_DATA.phone_number } )
					.then( function ( response ) {
						done( reponse );
					} )
					.catch( function ( error ) {
						expect( error.response.status ).to.equal( 401 );
						done();
					} );
			} );

			it( 'Returns status 202 for valid otp', function ( done ) {
				Axios.post( `${RESOURCE_URL}/activate`, { otp: '1234', phone_number: USER_DATA.phone_number } )
					.then( function ( response ) {
						expect( response.status ).to.equal( 202 );
						token = response.data.token;
						done();
					} )
					.catch( function ( error ) {
						done( error );
					} );
			} );

		} );
	} );

	describe( 'show: Get User', function () {

		it( 'Returns status 200 for existing User', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.get( `${RESOURCE_URL}/${userID}`, config )
				.then( function ( response ) {
					expect( response.status ).to.equal( 200 );

					expect( response.data.firstname ).to.equal( USER_DATA.firstname );
					expect( response.data.lastname ).to.equal( USER_DATA.lastname );
					expect( response.data.organization ).to.equal( USER_DATA.organization );
					expect( response.data.phone_number ).to.equal( USER_DATA.phone_number );

					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 404 for non-existent User', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.get( `${RESOURCE_URL}/${fakeUserID}`, config )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 404 );
					done();
				} );
		} );

		it( 'Returns status 401 for invalid Auth', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer invalid_token' } };

			Axios.get( `${RESOURCE_URL}/${userID}`, config )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 401 );
					done();
				} );
		} );
	} );

	describe( 'update: Update User', function () {

		it( 'Returns status 202 for good data', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.patch( `${RESOURCE_URL}/${userID}`, USER_DATA_UPDATE, config )
				.then( function ( response ) {

					expect( response.status ).to.equal( 202 );

					expect( response.data.firstname ).to.equal( USER_DATA_UPDATE.firstname );
					expect( response.data.lastname ).to.equal( USER_DATA_UPDATE.lastname );
					expect( response.data.organization ).to.equal( USER_DATA_UPDATE.organization );
					expect( response.data.phone_number ).to.equal( USER_DATA_UPDATE.phone_number );
					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 422 for for malformed data', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.patch( `${RESOURCE_URL}/${userID}`, USER_DATA_MALFORMED, config )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 422 );
					done();
				} );
		} );
	} );

	describe( 'destroy: Delete a user', function () {

		it( 'Returns status 404 for non-existent User', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.delete( `${RESOURCE_URL}/${fakeUserID}`, config )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 404 );
					done();
				} );
		} );

		it( 'Returns status 401 for invalid Authorization', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer invalid_token' } };

			Axios.delete( `${RESOURCE_URL}/${userID}`, config )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 401 );
					done();
				} );
		} );

		it( 'Returns status 202 for existing User', function ( done ) {

			const config = { headers: { 'Authorization': 'Bearer ' + token.token } };

			Axios.delete( `${RESOURCE_URL}/${userID}`, config )
				.then( function ( response ) {
					expect( response.status ).to.equal( 202 );
					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

	} );

} );