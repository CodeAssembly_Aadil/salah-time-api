var Chai = require( 'chai' );
var Axios = require( 'axios' );

var expect = Chai.expect;

const MOSQUE_DATA = {
	name: 'Eldoraigne Musallah',
	longitude: '-25.8490637',
	latitude: '28.1415175',
	province: 'Gauteng'
};

const MOSQUE_DATA_UPDATE = {
	name: 'Juma Masjid',
	longitude: '-29.8571927',
	latitude: '31.0161396',
	province: 'KwaZulu-Natal'
};

const MOSQUE_DATA_MALFORMED = {
	name: 'Eldoraigne Musallah',
	longitude: 'incorrect',
	latitude: '28.1415175',
	province: 'Incorrect'
};

const MOSQUE_TIME_DATA = {
	times: JSON.stringify(
		{
			fajr: {
				jamaat: true,
				adhan: '05:00',
				salah: '05:20'

			},
			dhuhr: {
				jamaat: true,
				adhan: '12:30',
				salah: '12:45'
			},
			jumuah: {
				jamaat: true,
				adhan: '12:30',
				salah: '13:00'
			},
			asr: {
				jamaat: true,
				adhan: '05:15',
				salah: '05:30'
			},
			magrib: {
				jamaat: true,
				adhan: '18:55',
				salah: '19:00'
			},
			isha: {
				jamaat: true,
				adhan: '20:15',
				salah: '20:30'
			}
		}
	)
};

const MOSQUE_TIME_DATA_MALFORMED = {
	times: JSON.stringify(
		{
			fajr: {
				jamaat: true,
				adhan: '05:00',
				salah: '05:20'

			},
			dhuhr: {
				// missing field
				adhan: '12:30',
				salah: '12:45'
			},
			jumuah: {
				jamaat: true,
				// missing field
				salah: '13:00'
			},
			asr: {
				jamaat: true,
				adhan: '05:15'
				// missing field
			},
			magrib: {
				jamaat: true,
				adhan: 'wrong',
				salah: '19:00'
			},
			isha: {
				jamaat: 'wrong',
				adhan: '20:15',
				salah: '20:30'
			}
		}
	)
};

let mosqueID = null;
let fakeMosqueID = 'abc';

const RESOURCE_URL = 'http://salahtime.develop/mosques';

describe( 'MosqueController', function () {

	describe( 'store: Create new Mosque', function () {

		it( 'Returns status 201 for new Mosque', function ( done ) {

			Axios.post( RESOURCE_URL, MOSQUE_DATA )
				.then( function ( response ) {
					expect( response.status ).to.equal( 201 );

					expect( response.data.name ).to.equal( MOSQUE_DATA.name );
					expect( response.data.longitude ).to.equal( MOSQUE_DATA.longitude );
					expect( response.data.latitude ).to.equal( MOSQUE_DATA.latitude );
					expect( response.data.province ).to.equal( MOSQUE_DATA.province );

					mosqueID = response.data.id;

					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 422 for new Mosque with malformed data', function ( done ) {

			Axios.post( RESOURCE_URL, MOSQUE_DATA_MALFORMED )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 422 );
					done();
				} );
		} );
	} );

	describe( 'MosqueTimesController', function () {

		describe( 'storeTimes: Add Mosque times', function () {

			it( 'Returns status 202 for good data', function ( done ) {

				Axios.patch( `${RESOURCE_URL}/${mosqueID}/times`, MOSQUE_TIME_DATA )
					.then( function ( response ) {
						expect( response.status ).to.equal( 202 );
						done();
					} )
					.catch( function ( error ) {
						console.error( error.response.data );
						done( error );
					} );
			} );

			// it( 'Returns status 422 for malformed data', function ( done ) {

			// 	Axios.patch( `${RESOURCE_URL}/${mosqueID}/times`, MOSQUE_TIME_DATA_MALFORMED )
			// 		.then( function ( response ) {
			// 			done( response );
			// 		} )
			// 		.catch( function ( error ) {
			// 			expect( error.response.status ).to.equal( 422 );
			// 			done();
			// 		} );
			// } );
		} );


		describe( 'showTimes: Get Mosque times', function () {

			it( 'Returns status 200 for existing Mosque', function ( done ) {

				Axios.get( `${RESOURCE_URL}/${mosqueID}/times` )
					.then( function ( response ) {
						expect( response.status ).to.equal( 200 );

						const matchData = JSON.parse( MOSQUE_TIME_DATA.times );

						expect( response.data.times.fajr.adhan ).to.equal( matchData.fajr.adhan );
						expect( response.data.times.isha.salah ).to.equal( matchData.isha.salah );

						done();
					} )
					.catch( function ( error ) {
						done( error );
					} );
			} );

			it( 'Returns status 404 for non-existent Mosque', function ( done ) {

				Axios.get( `${RESOURCE_URL}/${fakeMosqueID}/times` )
					.then( function ( response ) {
						done( response );
					} )
					.catch( function ( error ) {
						expect( error.response.status ).to.equal( 404 );
						done();
					} );
			} );
		} );
	} );

	describe( 'show: Get Mosque', function () {

		it( 'Returns status 200 for existing Mosque', function ( done ) {

			Axios.get( `${RESOURCE_URL}/${mosqueID}` )
				.then( function ( response ) {
					expect( response.status ).to.equal( 200 );
					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 404 for non-existent Mosque', function ( done ) {
			Axios.get( `${RESOURCE_URL}/${fakeMosqueID}` )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 404 );
					done();
				} );
		} );
	} );

	describe( 'update: Update Mosque', function () {

		it( 'Returns status 202 for good data', function ( done ) {

			Axios.patch( `${RESOURCE_URL}/${mosqueID}`, MOSQUE_DATA_UPDATE )
				.then( function ( response ) {
					expect( response.status ).to.equal( 202 );

					expect( response.data.name ).to.equal( MOSQUE_DATA_UPDATE.name );
					expect( response.data.longitude ).to.equal( MOSQUE_DATA_UPDATE.longitude );
					expect( response.data.latitude ).to.equal( MOSQUE_DATA_UPDATE.latitude );
					expect( response.data.province ).to.equal( MOSQUE_DATA_UPDATE.province );

					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 422 for for malformed data', function ( done ) {

			Axios.patch( `${RESOURCE_URL}/${mosqueID}`, MOSQUE_DATA_MALFORMED )
				.then( function ( response ) {
					done();
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 422 );
					done();
				} );
		} );
	} );

	describe( 'destroy: Delete a Mosque', function () {

		it( 'Returns status 202 for existing Mosque', function ( done ) {

			Axios.delete( `${RESOURCE_URL}/${mosqueID}` )
				.then( function ( response ) {
					expect( response.status ).to.equal( 202 );
					done();
				} )
				.catch( function ( error ) {
					done( error );
				} );
		} );

		it( 'Returns status 404 for non-existent Mosque', function ( done ) {

			Axios.delete( `${RESOURCE_URL}/${mosqueID}` )
				.then( function ( response ) {
					done( response );
				} )
				.catch( function ( error ) {
					expect( error.response.status ).to.equal( 404 );
					done();
				} );
		} );
	} );

} );