'use strict';

const Boom = require('boom');
const HttpStatusCodes = require('http-status-codes');
const Moment = require('moment');
const RandomString = require('randomstring');
const UUID = require('uuid/v4');

const DB = require.main.require('./app/lib/DB');
const Reply = require.main.require('./app/lib/response/Reply');

class AuthController {

    static async generateOTP(request, reply) {

        let user;

        console.log('phone_number', request.payload.phone_number);

        try {
            user = await DB('users')
                .where('phone_number', request.payload.phone_number)
                .first('uuid', 'phone_number', 'locked');

        } catch (error) {
            console.error(error);
            return reply(Boom.badImplementation());
        }

        if (!user) {
            return reply(Boom.unauthorized('Account not found'));
        } else if (user.locked) {
            return reply(Boom.unauthorized('Account locked'));
        }

        const otp = RandomString.generate({
            length: 8,
            readable: true,
            charset: 'alphanumeric',
            capitalization: 'uppercase'
        });

        const otpCreatedAt = Moment.utc();

        try {
            await DB('users')
                .where('uuid', user.uuid)
                .update({ otp, otp_created_at: otpCreatedAt, 'updated_at': Moment.utc() });

        } catch (error) {
            console.error(error);
            return reply(Boom.badImplementation());
        }

        return reply(new Reply(HttpStatusCodes.OK, 'OK', 'OTP sent'));
    }

    static async validateOTP(request, reply) {

        console.log('AuthController.validateOTP');

        let user;

        try {
            user = await DB('users')
                .where('phone_number', request.payload.phone_number)
                .first('uuid', 'firstname', 'lastname', 'organization', 'phone_number', 'otp', 'otp_created_at', 'locked');
        } catch (error) {
            console.error(error);
            return reply(Boom.badImplementation);
        }

        if (!user) {
            return reply(Boom.unauthorized('Account not found'));
        } else if (user.locked) {
            return reply(Boom.unauthorized('Account locked'));
        }

        const otpCreatedAt = Moment(user.otp_created_at);

        if (user.otp !== request.payload.otp || otpCreatedAt.isAfter(otpCreatedAt.clone().add(process.env.OTP_LIFETIME, 'seconds'))) {
            return reply(Boom.unauthorized('Invalid OTP'));
        }

        const token = `${UUID()}-${UUID()}`;
        const now = Moment.utc();

        // clear otp and add token
        try {
            await DB('users')
                .where('uuid', user.uuid)
                .update({ otp: null, otp_created_at: null, token, token_created_at: now, activated: true, updated_at: now });
        } catch (error) {
            return replay(Boom.badImplementation());
        }

        const credentials = Object.assign(Object.create(null), user);

        delete credentials.otp;
        delete credentials.otp_created_at;
        delete credentials.locked;

        credentials.token = token;
        credentials.token_created_at = now;
        credentials.token_lifetime = process.env.TOKEN_LIFETIME;

        return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Account Credentials', credentials));
    }
}

module.exports = AuthController;
