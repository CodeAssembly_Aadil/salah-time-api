'use strict';

const Joi = require('joi');

const PhoneNumberSchema = require.main.require('./app/lib/validation/PhoneNumberSchema');

// extent base shema
module.exports = PhoneNumberSchema.keys({
    otp: Joi.string().alphanum().length(8).required().label('OTP')
});
