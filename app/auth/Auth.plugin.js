'use strict';

// External Dependencies

const RandomString = require('randomstring');
const Moment = require('moment');

// Internal Dependencies

const AuthController = require('./AuthController');
const DB = require('../lib/DB');
const OTPSchema = require('./OTPSchema');
const PhoneNumberSchema = require('../lib/validation/PhoneNumberSchema');
const RouteValidationResponse = require('../lib/validation/RouteValidationResponse');

// Declare internals

const validationOptions = { abortEarly: false, presence: 'required' };

exports.register = function register(server, options, next) {

    server.auth.strategy('token', 'bearer-access-token', {

        async validateFunc(token, callback) {

            // For convenience, the request object can be accessed
            // from `this` within validateFunc.
            // let request = this;

            // console.log('request.params', request.params)

            let user;

            try {
                user = await DB('users')
                    .where('token', token)
                    .first('uuid', 'firstname', 'lastname', 'phone_number', 'locked', 'token', 'token_created_at');
            } catch (error) {
                console.error(error);
                return callback(error, false, null, null);
            }

            if (!user) {
                return callback(null, false, null, null);
            }

            const tokenCreatedAt = Moment(user.token_created_at);

            // Use a real strategy here,
            // comparing with a token from your database for example
            if (!user.locked && user.token && token === user.token && tokenCreatedAt.isBefore(tokenCreatedAt.clone().add(process.env.TOKEN_LIFETIME, 'seconds'))) {
                return callback(null, true, user, null);
            }

            return callback(null, false, null, null);
        }
    });

    server.route(
        [{
            method: 'POST',
            path: '/request-otp',
            handler: AuthController.generateOTP,
            config: {
                validate: {
                    params: false,
                    query: false,
                    payload: PhoneNumberSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'POST',
            path: '/validate-otp',
            handler: AuthController.validateOTP,
            config: {
                validate: {
                    params: false,
                    query: false,
                    payload: OTPSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }]
    );

    next();
};

exports.register.attributes = {
    name: 'Auth'
};
