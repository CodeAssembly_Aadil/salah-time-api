'use strict';

// External modules

const Boom = require('boom');
const Hoek = require('hoek');
const Joi = require('joi');

// Internal modules
const OTPSchema = require.main.require('./app/auth/OTPSchema');

// Declare internals

const internals = {};

internals.implementation = function(server, options) {
  Hoek.assert(options, 'Missing basic auth strategy options');
  Hoek.assert(
    typeof options.validateFunc === 'function',
    'options.validateFunc must be a valid function in basic scheme'
  );

  const settings = Hoek.clone(options);

  const scheme = {
    authenticate(request, reply) {
      console.log('OTPAuthSchema.authenticate');
      return reply.continue({ credentials: {} });
    },

    payload(request, reply) {
      console.log('OTPAuthSchema.payload');

      const validation = Joi.validate(request.payload, OTPSchema);

      if (validation.error) {
        return reply(
          Boom.unauthorized(
            'Missing authentication parameters',
            'OTP',
            settings.unauthorizedAttributes
          )
        );
      }

      const phoneNumber = request.payload.phone_number;
      const otp = request.payload.otp;

      settings.validateFunc(
        request,
        phoneNumber,
        otp,
        (err, isValid, credentials) => {
          // credentials = credentials || null;

          if (err) {
            return reply(err, null, { credentials });
          }

          if (!isValid) {
            return reply(
              Boom.unauthorized('Bad Phone Number or invalid OTP', 'OTP'),
              null,
              { credentials }
            );
          }
          console.log(credentials);

          // if (!credentials || Object.prototype.toString.call(credentials) !== 'object') {
          if (!credentials || typeof credentials !== 'object') {
            return reply(
              Boom.unauthorized(
                'Bad credentials object received for OTP Auth validation'
              ),
              'OTP',
              settings.unauthorizedAttributes
            );

            // return reply(Boom.badImplementation('Bad credentials object received for OTP Auth validation', 'OTP', settings.unauthorizedAttributes));
          }

          // Authenticated

          return reply.continue({ credentials });
        }
      );
    }
  };

  return scheme;
};

exports.register = function register(plugin, options, next) {
  plugin.auth.scheme('phone-otp', internals.implementation);
  next();
};

exports.register.attributes = {
  // pkg: require('../package.json')
  name: 'PhoneOTPAuth',
  version: require.main.require('./package.json').version
};
