'use strict';

const Boom = require('boom');
const HttpStatusCodes = require('http-status-codes');
const Moment = require('moment');

const GoogleMaps = require.main.require('./app/lib/GoogleMaps');
const DB = require.main.require('./app/lib/DB');
const Reply = require.main.require('./app/lib/response/Reply');
const PrayerTimes = require.main.require('./app/mosque/PrayerTimes');
const Mosque = require.main.require('./app/mosque/Mosque');

class MosqueController {
	static async search(request, reply) {
		reply('MosqueController.search').code(HttpStatusCodes.NOT_IMPLEMENTED);
	}

	static async show(request, reply) {
		let mosque = null;

		try {
			mosque = await Mosque.fetchBySlug(request.params.slug);
		} catch (error) {
			console.error(error);
			return reply(Boom.badImplementation());
		}

		if (mosque === null) {
			return reply(Boom.notFound('Mosque not found'));
		}

		return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Mosque', mosque.toObject())).code(HttpStatusCodes.OK);
	}

	static async store(request, reply) {
		const name = request.payload.name;
		const building = request.payload.building;
		const longitude = request.payload.longitude;
		const latitude = request.payload.latitude;

		try {
			const mosque = await Mosque.create(name, building, longitude, latitude);
			await mosque.save();

			return reply(new Reply(HttpStatusCodes.CREATED, 'Created', 'Mosque created', mosque.toObject())).code(HttpStatusCodes.CREATED);
		} catch (error) {

			if (error.message.includes('duplicate key value violates unique constraint')) {
				return reply(Boom.badData('Mosque is already registered'));
			}

			return reply(Boom.badImplementation('Could not add new Mosque'));
		}
	}

	static async update(request, reply) {
		const name = request.payload.name;
		const building = request.payload.building;
		const longitude = request.payload.longitude;
		const latitude = request.payload.latitude;

		try {
			const mosque = await Mosque.fetchByUUID(request.params.uuid);

			if (!mosque) {
				return reply(Boom.notFound('Mosque not found'));
			}

			mosque.update(name, building, longitude, latitude);
			mosque.save();

			return reply(new Reply(HttpStatusCodes.OK, 'Created', 'Mosque created', mosque.toObject())).code(HttpStatusCodes.OK);
		} catch (error) {
			if (error.message.includes('duplicate key value violates unique constraint')) {
				return reply(Boom.badData('A Mosque with these details already exisits'));
			}

			return reply(Boom.badImplementation('Could not update Mosque'));
		}
	}

	static async destroy(request, reply) {
		reply('MosqueController.destroy').code(HttpStatusCodes.NOT_IMPLEMENTED);
	}

	static async showTimes(request, reply) {
		try {
			const mosque = await Mosque.fetchBySlug(request.params.slug);

			if (!mosque) {
				return reply(Boom.notFound('Mosque not found'));
			}

			return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Mosque', mosque.times)).code(HttpStatusCodes.OK);
		} catch (error) {
			console.error(error);
		}

		return reply(Boom.badImplementation());
	}

	static async updateTimes(request, reply) {
		reply('MosqueController.storeTimes').code(HttpStatusCodes.NOT_IMPLEMENTED);
	}
}

module.exports = MosqueController;
