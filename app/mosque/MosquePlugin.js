'use strict';

const MosqueController = require.main.require('./app/mosque/MosqueController');
const MosqueSchema = require.main.require('./app/mosque/MosqueSchema');
const PrayerSchema = require.main.require('./app/mosque/PrayerSchema');
const RouteValidationResponse = require.main.require('./app/lib/validation/RouteValidationResponse');
const UUIDV4Schema = require.main.require('./app/lib/validation/UUIDV4Schema');
const SlugSchema = require.main.require('./app/lib/validation/SlugSchema');

const validationOptions = { abortEarly: false, presence: 'required' };

exports.register = function (server, options, next) {

    server.route(
        [{
            method: 'GET',
            path: '/mosque/search',
            handler: MosqueController.search,
            config: {
                auth: null,
                validate: {
                    params: false,
                    query: true,
                    payload: false,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'GET',
            path: '/mosque/{slug}',
            handler: MosqueController.show,
            config: {
                auth: null,
                validate: {
                    params: SlugSchema,
                    query: false,
                    payload: false,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'POST',

            path: '/mosque',
            handler: MosqueController.store,
            config: {
                auth: 'token',
                validate: {
                    params: false,
                    query: false,
                    payload: MosqueSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'PATCH',
            path: '/mosque/{uuid}',
            handler: MosqueController.update,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: MosqueSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'DELETE',
            path: '/mosque/{uuid}',
            handler: MosqueController.destroy,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: false,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        },
        {
            method: 'GET',
            path: '/mosque/{uuid}/times',
            handler: MosqueController.showTimes,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: false,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        },
        {
            method: 'PATCH',
            path: '/mosque/{uuid}/times',
            handler: MosqueController.updateTimes,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: PrayerSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }]
    );

    next();
};

exports.register.attributes = {
    name: 'Mosque',
    version: require.main.require('./package').version
};
