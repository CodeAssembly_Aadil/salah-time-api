'use strict';

const Joi = require('joi');

const PrayerSchema = Joi.object().keys({
    name: Joi.string().required().label('Salah Name'),
    jamaat: Joi.boolean().required().label('Jamaat'),
    adhan: Joi.string().regex(/^([01][0-9]|2[0-3]):[0-5][0-9]$/).required().label('Adhan Time'),
    salah: Joi.string().regex(/^([01][0-9]|2[0-3]):[0-5][0-9]$/).required().label('Salah Time')
});


module.exports = Joi.object().keys({
    prayers: Joi.array().length(6).ordered(
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Fajr').required()
            })
        ),
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Dhuhr').required()
            })
        ),
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Jumuah').required()
            })
        ),
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Asr').required()
            })
        ),
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Maghrib').required()
            })
        ),
        PrayerSchema.concat(
            Joi.object({
                name: Joi.any().valid('Isha').required()
            })
        )
    ).required().label('Prayer Times')
});
