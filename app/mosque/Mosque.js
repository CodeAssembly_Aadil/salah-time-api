'use strict';

const Moment = require('moment');
const Lodash = require('lodash');

const Slug = require.main.require('./app/lib/Slug');
const DB = require.main.require('./app/lib/DB');
const GoogleMaps = require.main.require('./app/lib/GoogleMaps');
const PrayerTimes = require.main.require('./app/mosque/PrayerTimes');

class Mosque {
	constructor() {
		this._uuid = null;
		this._slug = null;

		this.name = null;

		this.building = null;

		this.street_number = null;
		this.route = null;
		this.sublocality = null;
		this.locality = null;
		this.postal_code = null;
		this.province = null;
		this.country = null;

		this._longitude = null;
		this._latitude = null;

		this.place_id = null;

		this.times = null;
		this.times_updated_at = null;

		this.created_at = Moment.utc();
		this.updated_at = null;

		Object.seal(this);
	}

	static get databaseTableName() {
		return 'mosques';
	}

	static async create(name, building, longitude, latitude) {
		const mosque = new Mosque();

		mosque.name = name;
		mosque.building = building;
		await mosque.fetchLocationData(longitude, latitude);
		mosque.setPrayerTimes(new PrayerTimes(mosque.longitude, mosque.latitude));
		mosque.generateSlug();

		return mosque;
	}

	static async fetchByUUID(uuid) {
		const mosqueData = await DB(Mosque.databaseTableName).where('uuid', '=', uuid).first('*', DB.postgis.asGeoJSON('location'));

		if (mosqueData === null) {
			return null;
		}

		const mosque = new Mosque();
		mosque.hydrateFromDBData(mosqueData);

		return mosque;
	}

	static async fetchBySlug(slug) {
		const mosqueData = await DB(Mosque.databaseTableName).where('slug', '=', slug).first('*', DB.postgis.asGeoJSON('location'));

		console.log(mosqueData);
		if (mosqueData === null) {
			return null;
		}

		const mosque = new Mosque();
		mosque.hydrateFromDBData(mosqueData);

		return mosque;
	}

	get uuid() {
		return this._uuid;
	}

	get slug() {
		return this._slug;
	}

	get longitude() {
		return this._longitude;
	}

	get latitude() {
		return this._latitude;
	}

	get createdAt() {
		if (!this.created_at) {
			return null;
		}
		return Moment(this.created_at);
	}

	get updatedAt() {
		if (!this.updated_at) {
			return null;
		}
		return Moment(this.updated_at);
	}

	async save() {
		const isUpdating = this.uuid !== null;
		const mosqueData = this.prepareForQuery(isUpdating);

		let dbMosqueData;

		if (isUpdating) {
			dbMosqueData = await DB(Mosque.databaseTableName).where('uuid', '=', this.uuid).update(mosqueData, ['*', DB.postgis.asGeoJSON('location')]);
		} else {
			dbMosqueData = await DB(Mosque.databaseTableName).insert(mosqueData, '*', DB.postgis.asGeoJSON('location'));
		}

		dbMosqueData = dbMosqueData[0];

		// dbMosqueData.location = JSON.parse(dbMosqueData.location);

		this.hydrateFromDBData(dbMosqueData);
	}

	computePrayerTimes() {
		this.times = new PrayerTimes(this.longitude, this.latitude);
		this.times_updated_at = Moment.utc();
	}

	setPrayerTimes(times) {
		this.times = times;
		this.times_updated_at = Moment.utc();
	}

	generateSlug() {
		// const street = this.route;
		const locality = this.sublocality || this.locality;
		// this._slug = Slug(`${this.name} ${street} ${locality}`);
		this._slug = Slug(`${this.name} ${locality}`);
	}

	setSlug(value) {
		this._slug = value;
	}

	async fetchLocationData(longitude, latitude) {
		const address = await GoogleMaps.fetchGeocodeData(longitude, latitude);

		if (!address) {
			throw new Error('Location not found');
		}

		Object.assign(this, Lodash.omit(address, ['longitude', 'latitude']));

		this._longitude = address.longitude;
		this._latitude = address.latitude;
	}

	async update(name, building, longitude, latitude) {
		this.name = name;
		this.building = building;

		if (this.longitude !== longitude || this.latitude !== latitude) {
			await this.fetchLocationData(longitude, latitude);
		}

		this.generateSlug();
	}

	hydrateFromDBData(data) {
		if (this.uuid === null) {
			Object.defineProperty(this, '_uuid', {
				value: data.uuid,
				writable: false,
				enumerable: true,
				configurable: false
			});
		}

		this._slug = data.slug;

		this.name = data.name;

		this.building = data.building;

		this.street_number = data.street_number;
		this.route = data.route;
		this.sublocality = data.sublocality;
		this.locality = data.locality;
		this.postal_code = data.postal_code;
		this.province = data.province;
		this.country = data.country;

		const location = JSON.parse(data.location);

		this._longitude = location.coordinates[1];
		this._latitude = location.coordinates[0];

		this.place_id = data.place_id;

		this.times = data.times;
		this.times_updated_at = data.times_updated_at;

		Object.defineProperty(this, 'created_at', {
			value: data.created_at,
			writable: false,
			enumerable: true,
			configurable: false
		});

		this.updated_at = data.updated_at;
	}

	prepareForQuery(isUpdating = false) {
		const dbData = Object.assign(Object.create(null), this);

		delete dbData._uuid;
		delete dbData._slug;
		delete dbData._longitude;
		delete dbData._latitude;
		// delete dbData.times;

		dbData.slug = this.slug;

		dbData.location = DB.postgis.geomFromText(`Point(${this.longitude} ${this.latitude})`, 4326);

		if (isUpdating) {
			dbData.updated_at = Moment.utc();
		}

		Object.freeze(dbData);

		return dbData;
	}

	toObject(fields = []) {
		let mosque = Object.assign(Object.create(null), this);

		mosque = Lodash.unset(mosque, ['_uuid', '_slug', '_longitude', '_latitude']);

		if (fields.length !== 0) {
			mosque = Lodash.pick(mosque, fields);
		} else {
			mosque.uuid = this.uuid;
			mosque.slug = this.slug;
			mosque.longitude = this.longitude;
			mosque.latitude = this.latitude;
		}

		Object.freeze(mosque);

		return mosque;
	}
}

module.exports = Mosque;
