'use strict';

const { isNumber } = require('lodash');

const PrayerTimesCalculator = require('prayer-times');
// const PrayerTimes = require('prayer-times');
const Moment = require('moment');

const Prayer = require.main.require('./app/mosque/Prayer');

const Calculator = new PrayerTimesCalculator();
Calculator.setMethod('Makkah');

class PrayerTimes {
  constructor(longitude = null, latitude = null, timezone = 2) {
    this.prayers = [];

    this.prayers.push(new Prayer('Fajr', false, '00:00', '00:00'));
    this.prayers.push(new Prayer('Dhuhr', false, '00:00', '00:00'));
    this.prayers.push(new Prayer('Jumuah', false, '00:00', '00:00'));
    this.prayers.push(new Prayer('Asr', false, '00:00', '00:00'));
    this.prayers.push(new Prayer('Maghrib', false, '00:00', '00:00'));
    this.prayers.push(new Prayer('Isha', false, '00:00', '00:00'));

    if (isNumber(longitude) && isNumber(latitude)) {
      this.computeTimesAtLocation(longitude, latitude, timezone);
    }
  }

  computeTimesAtLocation(longitude, latitude, timezone = 2) {
    // default to south african time;.
    const prayerTimes = Calculator.getTimes(
      new Date(),
      [latitude, longitude],
      timezone,
      '24h'
    );

    this.prayers.forEach(prayer => {
      const adhanTime = prayerTimes[prayer.name.toLowerCase()];

      if (adhanTime) {
        const adhanTimeOffsetMin = prayer.name === 'Magrib' ? 5 : 15;

        const salahTime = Moment(adhanTime, 'HH:mm')
          .add(adhanTimeOffsetMin, 'minutes')
          .format('HH:mm');

        prayer.jamaat = true;
        prayer.adhan = adhanTime;
        prayer.salah = salahTime;
      }
    });

    // Manually edit Jumuah
    const jumuah = this.prayers[2];
    const dhuhr = this.prayers[1];

    jumuah.jamaat = true;
    // Jumuah first adhan is typically 20-30 mins before salah
    jumuah.adhan = dhuhr.adhan;
    jumuah.salah = Moment(dhuhr.adhan, 'HH:mm')
      .add(30, 'minutes')
      .format('HH:mm');
  }
}

module.exports = PrayerTimes;
