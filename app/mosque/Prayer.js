'use strict';

class Prayer {
    constructor(name, jamaat, adhan, salah) {
        this.name = name;
        this.jamaat = jamaat;
        this.adhan = adhan;
        this.salah = salah;
    }
}

module.exports = Prayer;
