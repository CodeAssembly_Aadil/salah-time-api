'use strict';

const Joi = require('joi');

module.exports = Joi.object().keys({

    name: Joi.string().regex(/^[a-zA-Z0-9_ -]{1,255}$/).required().label('Name'),

    building: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,255}$/).optional().label('Building'),
    // street_number: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,32}$/).optional().label('Street Number'),
    // street: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,255}$/).optional().label('Street'),
    // sublocality: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,255}$/).optional().label('Sub Locality'),
    // locality: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,255}$/).optional().label('Locality'),
    // postal_code: Joi.string().allow('').regex(/^[0-9]{0,32}$/).optional().label('Postal Code'),
    // province: Joi.string().valid(['Eastern Cape', 'Free State', 'Gauteng', 'KwaZulu-Natal', 'Limpopo', 'Mpumalanga', 'Northern Cape', 'North West', 'Western Cape']).required().label('Province'),
    // country: Joi.string().valid(['South Africa']).required(),

    // longitude: Joi.string().regex(/^([-+]?((180(\.0+)?)|((1[0-7][0-9])|([1-9]?[0-9]))(\.[0-9]+)?))$/).required(),
    longitude: Joi.number().min(-180).max(180).required(),
    // latitude: Joi.string().regex(/^([-+]?((90(\.0+)?)|([1-8]?[0-9])(\.[0-9]+)?))$/).required(),
    latitude: Joi.number().min(-90).max(90).required(),

    // place_id: Joi.string().regex(/^[a-zA-Z0-9_ -]*$/).max(255).label('Google Maps Place ID')
});
