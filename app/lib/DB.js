'use strict';

const KnexPostgis = require('knex-postgis');
const Knex = require('knex');

const DatabaseConfig = require.main.require('./config/database');

const DB = Knex(DatabaseConfig);
KnexPostgis(DB);

module.exports = DB;

DB.migrate.latest(DatabaseConfig);
