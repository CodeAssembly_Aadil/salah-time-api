'use strict';

const GoogleMaps = require('@google/maps');
const HttpStatusCodes = require('http-status-codes');

const GOOGLEMAPS_API_KEY = process.env.GOOGLEMAPS_API_KEY;

const googleMapsClient = GoogleMaps.createClient({
	Promise,
	key: GOOGLEMAPS_API_KEY
});

module.exports = {
	fetchGeocodeData(longitude, latitude) {
		const query = { latlng: [longitude, latitude] };
		return googleMapsClient.reverseGeocode(query).asPromise().then(response => {
			if (response.status === HttpStatusCodes.OK) {
				const result = response.json.results[0];

				if (result) {
					return this.processReverseGeocodeResult(result);
				}
			}

			return null;
		});
	},

	processReverseGeocodeResult(gecoderResult) {
		const addressComponents = gecoderResult.address_components;


		console.log(gecoderResult)

		const addressParts = {};

		addressComponents.forEach(component => {
			component.types.forEach(addressType => {
				addressParts[addressType] = component.long_name;
			});
		});

		const address = Object.create(null);

		address.street_number = addressParts.street_number;
		address.route = addressParts.route;
		address.sublocality = addressParts.sublocality;
		address.locality = addressParts.locality;
		address.province = addressParts.administrative_area_level_1;
		address.country = addressParts.country;
		address.postal_code = addressParts.postal_code;

		address.place_id = gecoderResult.place_id;

		address.longitude = gecoderResult.geometry.location.lng;
		address.latitude = gecoderResult.geometry.location.lat;

		Object.freeze(address);

		return address;
	}
};
