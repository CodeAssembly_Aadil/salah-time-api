'use strict';

const Joi = require('joi');

module.exports = Joi.object().keys({
  slug: Joi.string().regex(/[a-z0-9-]{1,255}$/).required().label('Slug')
});
