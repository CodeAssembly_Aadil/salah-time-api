'use strict';

const Boom = require('boom');

module.exports = function RouteValidationResponse(request, reply, source, error) {
    if (source === 'params') {
        reply(Boom.badRequest('Invalid request'));
    } else if (source === 'payload') {
        const errorResponse = Boom.badData('Validation Errors', error.data.details);
        errorResponse.output.payload.data = error.data.details;
        reply(errorResponse);
    }
};
