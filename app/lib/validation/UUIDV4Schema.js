'use strict';

const Joi = require('joi');

module.exports = Joi.object().keys({
  uuid: Joi.string().guid({
    version: ['uuidv4']
  })
});
