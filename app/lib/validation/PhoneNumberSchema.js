'use strict';

const Joi = require('joi');

module.exports = Joi.object().keys({
    phone_number: Joi.string().regex(/^0[0-9]{9}$/).required().label('Phone Number')
});
