const Slug = require('slug');

Slug.defaults.modes.rfc3986 = {
  replacement: '-', // replace spaces with replacement
  symbols: true, // replace unicode symbols or not
  //   remove: /[^a-zA-Z0-9 ]/, // (optional) regex to remove characters
  remove: null, // (optional) regex to remove characters
  lower: true, // result in lower case
  charmap: Slug.charmap, // replace special characters
  multicharmap: Slug.multicharmap // replace multi-characters
};

Slug.defaults.mode = 'rfc3986';

module.exports = Slug;