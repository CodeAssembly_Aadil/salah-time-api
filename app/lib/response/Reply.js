'use strict';

class Reply {
    constructor(statusCode = 200, status = 'OK', message, data) {
        this.statusCode = statusCode;
        this.status = status;
        this.message = message;
        this.data = data;
    }
}

module.exports = Reply;
