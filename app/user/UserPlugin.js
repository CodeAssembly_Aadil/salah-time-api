'use strict';

const UUIDV4Schema = require.main.require('./app/lib/validation/UUIDV4Schema');
const RouteValidationResponse = require.main.require('./app/lib/validation/RouteValidationResponse');
const UserController = require.main.require('./app/user/UserController');
const UserSchema = require.main.require('./app/user/UserSchema');

const validationOptions = { abortEarly: false, presence: 'required' };

exports.register = function (server, options, next) {

    server.route(
        [{
            method: 'POST',
            path: '/user/register',
            handler: UserController.store,
            config: {
                validate: {
                    params: false,
                    query: false,
                    payload: UserSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'GET',
            path: '/user/{uuid}',
            handler: UserController.show,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: false,
                    failAction: RouteValidationResponse
                }
            }
        }, {
            method: 'PATCH',
            path: '/user/{uuid}',
            handler: UserController.update,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: UserSchema,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }, {
            method: 'DELETE',
            path: '/user/{uuid}',
            handler: UserController.destroy,
            config: {
                auth: 'token',
                validate: {
                    params: UUIDV4Schema,
                    query: false,
                    payload: false,
                    failAction: RouteValidationResponse,
                    options: validationOptions
                }
            }
        }]
    );

    next();
};

exports.register.attributes = {
    name: 'User',
    version: require.main.require('./package.json').version
};
