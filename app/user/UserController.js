'use strict';

const Boom = require('boom');
const HttpStatusCodes = require('http-status-codes');
const Moment = require('moment');

const DB = require.main.require('./app/lib/DB');
const Reply = require.main.require('./app/lib/response/Reply');

class UserController {
	static async show(request, reply) {
		if (request.auth.credentials.uuid !== request.params.uuid) {
			return reply(Boom.unauthorized());
		}

		let user = null;

		try {
			user = await DB('users').where('uuid', '=', request.params.uuid).first('uuid', 'firstname', 'lastname', 'organization', 'phone_number', 'created_at', 'updated_at');
		} catch (error) {
			console.error(error);
			return reply(Boom.badImplementation());
		}

		if (!user) {
			return reply(Boom.notFound('Account not found'));
		}

		return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Account', user)).code(HttpStatusCodes.OK);
	}

	static async store(request, reply) {
		try {
			const userData = Object.assign({}, request.payload);

			const user = await DB('users').insert(user).returning(['uuid', 'firstname', 'lastname', 'organization', 'phone_number', 'created_at', 'updated_at']);

			return reply(new Reply(HttpStatusCodes.CREATED, 'Created', 'Account registered', user[0])).code(HttpStatusCodes.CREATED);
		} catch (error) {
			if (error.message.includes('duplicate key value violates unique constraint')) {
				return reply(Boom.badData('User is already registered'));
			}
		}

		return reply(Boom.badImplementation('Could not register new Account'));
	}

	static async update(request, reply) {
		if (request.auth.credentials.uuid !== request.params.uuid) {
			return reply(Boom.unauthorized());
		}

		try {
			const user = await DB('users')
				.where('uuid', '=', request.params.uuid)
				.update({
					firstname: request.payload.firstname,
					lastname: request.payload.lastname,
					organization: request.payload.organization,
					updated_at: Moment.utc()
				})
				.returning(['uuid', 'firstname', 'lastname', 'organization', 'phone_number', 'created_at', 'updated_at']);

			if (!user[0]) {
				return reply(Boom.notFound('Account not found'));
			}

			return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Account updated', user[0])).code(HttpStatusCodes.OK);
		} catch (error) {
			console.error(error);
		}
		return reply(Boom.badImplementation('Could not update Account'));
	}

	static async destroy(request, reply) {
		if (request.auth.credentials.uuid !== request.params.uuid) {
			return reply(Boom.unauthorized());
		}

		let deleteResult = null;

		try {
			deleteResult = await DB('users').where('uuid', '=', request.params.uuid).del();
		} catch (error) {
			console.error(error);
			return reply(Boom.badImplementation());
		}

		if (deleteResult !== 1) {
			return reply(Boom.notFound('Account not found'));
		}

		return reply(new Reply(HttpStatusCodes.OK, 'OK', 'Account removed')).code(HttpStatusCodes.OK);
	}
}

module.exports = UserController;
