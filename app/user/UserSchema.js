'use strict';

const Joi = require('joi');

const PhoneNumberSchema = require.main.require('./app/lib/validation/PhoneNumberSchema');

module.exports = PhoneNumberSchema.keys({
    firstname: Joi.string().regex(/^[a-zA-Z0-9_ -]{1,255}$/).required().label('Firstname'),
    lastname: Joi.string().regex(/^[a-zA-Z0-9_ -]{1,255}$/).required().label('Lastname'),
    organization: Joi.string().allow('').regex(/^[a-zA-Z0-9_ -]{0,255}$/).optional().label('Organization'),
    // phone_number: Joi.string().regex(/^0[0-9]{9}$/).required().label('Phone Number'),
    accepted_terms: Joi.boolean().invalid(false).required().label('Terms and Conditions')
});
