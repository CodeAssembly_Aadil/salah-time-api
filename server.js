'use strict';

// if (process.env.NODE_ENV !== 'production') {
//     require('@glimpse/glimpse').init();
// }

const DotEnv = require('dotenv');
const Glue = require('glue');
const Bluebird = require('bluebird');

// replace native promises
global.Promise = Bluebird;

DotEnv.config();

const DB = require.main.require('./app/lib/DB');

const manifest = {
	// server: {
	// 	cache: 'redis'
	// },
	connections: [
		{
			address: process.env.ADDRESS,
			port: process.env.PORT,
			labels: ['api']
		}

	],
	registrations: [
		{
			plugin: 'hapi-es7-async-handler'
		},
		{
			plugin: 'vision'
		},
		{
			plugin: 'inert'
		},
		{
			plugin: 'lout'
		},
		{
			plugin: 'hapi-auth-bearer-token'
		},
		{
			plugin: './app/auth/OTPAuthScheme'
		},
		{
			plugin: './app/auth/AuthPlugin.js'
		},
		{
			plugin: './app/mosque/MosquePlugin.js'
		},
		{
			plugin: './app/user/UserPlugin.js'
		}
	]
};

const options = {
	relativeTo: __dirname
};

Glue.compose(manifest, options, (error, server) => {

	if (error) {
		console.error(error);
		throw error;
	}

	server.start(() => {
		console.log(`Server running at: ${server.info.uri}`);
	});
});
