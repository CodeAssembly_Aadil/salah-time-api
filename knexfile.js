// Update with your config settings.
const DotEnv = require('dotenv');

// DotEnv.config({ path: '../.env' });
DotEnv.config();

const config = {
  client: 'pg',
  connection: {
    host: 'api.salahtime.develop',
    // host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: './database/migrations'
  }
}

console.log(config);

module.exports = config;
